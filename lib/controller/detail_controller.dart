import 'package:flutter/material.dart';
import 'package:tutomap/model/remarquable_place.dart';

class DetailController extends StatelessWidget{
  final RemarquablePlace place;
  DetailController({required this.place});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: const Text("Details"),),
      body: Center(
        child: Column(
          children: [
            Text(place.city),
            Text(place.adress),
            Text("Coords: ${place.lat} / ${place.lon}")
          ],
        ),
      ),
    );
  }
}