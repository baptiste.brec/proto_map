import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NoDataController extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Text(
          "Aucune donnée pour l'instant",
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 30.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}