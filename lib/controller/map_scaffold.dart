import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:tutomap/controller/detail_controller.dart';
import 'package:tutomap/map/customMarker.dart';
import 'package:tutomap/model/remarquable_place.dart';
import 'package:tutomap/services/datas_manager.dart';
import 'package:tutomap/services/location_manager.dart';
import 'package:tutomap/view/app_bar.dart';
import 'package:tutomap/view/drawer_view.dart';
import 'package:tutomap/view/map_view.dart';

import '../model/map_type.dart';

import 'package:flutter_osm_plugin/flutter_osm_plugin.dart' as osm;

class MapScaffold extends StatefulWidget{
  final Position startPosition;
  const MapScaffold({Key? key, required this.startPosition}): super (key: key);


  @override
  MapState createState() => MapState();
}

class MapState extends State<MapScaffold>{

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  MapType mapType = MapType.simple;

  MapController mapController = MapController();
  double zoom=12;
  LatLng center = LatLng(0, 0);
  late StreamSubscription<Position> subscription;
  Stream<Position> listener=LocationManager().positionListener();
  bool shouldFollow=true;
  Icon get followIcon => Icon((shouldFollow)? Icons.location_on : Icons.location_off, color: (shouldFollow)? Colors.white : Colors.red);

  List<Marker> markers = [];

  @override
  void initState() {
    super.initState();
    center= LatLng(widget.startPosition.latitude, widget.startPosition.longitude);
    observePositionChanges();
    getMarkers();
  }

  @override
  void dispose() {
    stopObserving();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBarView(
        context: context,
        menuPressed: menuPressed,
        getPosition: getPosition,
        zoomIn: zoomIn,
        zoomOut: zoomOut,
        followPosition: followPosition,
        followIcon: followIcon,
      ),
      drawer: DrawerView(onDrawerTap: onDrawerTap,),
      body: MapView(
          mapController: mapController,
         center: center,
          zoom: zoom,
        mapType: mapType,
        markers: markers,
        moveToDetail: moveToDetail,
        removeMarker: removeMarker,
      )
    );
  }

  observePositionChanges(){
    subscription=listener.listen((newPosition) {
      updatePosition(newPosition);
    });
    mapController.mapEventStream.listen((event) {
      if (event is MapEventLongPress){
        final LatLng tap = event.tapPosition;
        final lat = tap.latitude;
        final lon = tap.longitude;
        final RemarquablePlace newPlace = RemarquablePlace(lat: lat, lon: lon, city: "", adress: "");
        final String toBeSave = newPlace.toBeSavedString;
        DatasManager().saveDatas(toBeSave).then((_) => getMarkers());
        /*
        final Marker newMarker= Marker(
            point: event.tapPosition,
            builder: (context){
              return Icon(Icons.local_airport_outlined, color: Colors.orange,);
            }
        );
        setState(() {
          markers.add(newMarker);
        });
        //ajouter aux shared
        print(event.tapPosition);
        
         */
      }
      zoom = mapController.zoom;
    });
  }

  /*tracerRoute() async {
    var route = osm.RoadInfo(
      route: [
        osm.GeoPoint(latitude: center.latitude, longitude: center.longitude),
        osm.GeoPoint(latitude: 47.4371, longitude: 8.6136),
      ]
    );

    Future<osm.RoadInfo> drawRoad(
        osm.GeoPoint start,
        osm.GeoPoint end, {
          osm.RoadType roadType = osm.RoadType.bike,
          List<osm.GeoPoint>? intersectPoint,
          osm.RoadOption? roadOption,
        }) async {
      return await route.drawRoad(
        start,
        end,
        roadType: roadType,
        interestPoints: intersectPoint,
        roadOption: roadOption,
      );
    }





    osm.RoadInfo roadInfo = await mapController.drawRoad(
      osm.GeoPoint(latitude: center.latitude, longitude: center.longitude),
      osm.GeoPoint(latitude: 47.4371, longitude: 8.6136),
      roadColor : Colors.green,
      roadWidth : 7.0,
    );
    print("${roadInfo.distance}km");
    print("${roadInfo.duration}sec");
  }*/


  getMarkers() async {
    final newMarkers = await DatasManager().getDatas();
    final list = newMarkers.map((e) => CustomMarker(savedString: e)).toList();
    setState(() => markers = list);
  }
  
  updatePosition(Position position){
    center = LatLng(position.latitude, position.longitude);
    mapController.move(center, zoom);
  }

  stopObserving(){
    subscription.cancel();
  }

  menuPressed(){
    scaffoldKey.currentState?.openDrawer();
  }

  followPosition(){
    setState(() {
      shouldFollow=!shouldFollow;
      shouldFollow? observePositionChanges(): stopObserving();
    });
  }

  getPosition(){
    setState(() {
      mapController.move(center, zoom);
    });
  }

  zoomIn(){
    setState(() {
      zoom=mapController.zoom+0.5;
      mapController.move(mapController.center,zoom);
    });
  }

  zoomOut(){
    setState(() {
      zoom=mapController.zoom-0.5;
      mapController.move(mapController.center,zoom);
    });
  }

  onDrawerTap(MapType mapType){
    setState(() {
      this.mapType=mapType;
      Navigator.of(context).pop();
    });
  }

  removeMarker(String str){
    DatasManager().deleteDatas(str).then((_) => getMarkers());
  }

  moveToDetail(RemarquablePlace place){
    final route = MaterialPageRoute(builder: (ctx) => DetailController(place: place));
    Navigator.push(context,route);
  }
}