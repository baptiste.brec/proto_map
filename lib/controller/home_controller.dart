import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tutomap/controller/map_scaffold.dart';
import 'package:tutomap/controller/no_data_controller.dart';
import 'package:tutomap/services/location_manager.dart';

class HomeController extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Position>(
      future: LocationManager().start(),
        builder: ((context, position) => (position.hasData) ? MapScaffold(startPosition: position.data!,): NoDataController())
    );
  }
}