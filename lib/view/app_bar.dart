import 'package:flutter/material.dart';

class AppBarView extends PreferredSize{
  final BuildContext context;
  final Function() menuPressed;
  final Function() getPosition;
  final Function() zoomIn;
  final Function() zoomOut;
  final Function() followPosition;
  final Icon followIcon;


  AppBarView({
    required this.context,
    required this.menuPressed,
    required this.getPosition,
    required this.zoomIn,
    required this.zoomOut,
    required this.followPosition,
    required this.followIcon,

}): super(
    preferredSize: Size.fromHeight(125),
    child: Container(
      color: Theme.of(context).primaryColorDark,
      child: SafeArea(
        child: Column(
          children: [
            Padding(
                padding: const EdgeInsets.all(5),
              child: Row(
                children: [
                  IconButton(
                      onPressed: menuPressed,
                      icon: const Icon(Icons.menu),
                      color: Colors.white,
                  ),
                  const Text("Learning maps",
                      style: TextStyle(
                          color: Colors.white,
                        fontStyle: FontStyle.italic,
                        fontSize: 20
                      )
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(onPressed: getPosition, icon: const Icon(Icons.my_location_rounded),color: Colors.white,),
                  IconButton(onPressed: zoomIn, icon: const Icon(Icons.zoom_in),color: Colors.white,),
                  IconButton(onPressed: zoomOut, icon: const Icon(Icons.zoom_out),color: Colors.white,),
                  IconButton(onPressed: followPosition, icon: followIcon),
                ],
              ),
            )
          ],
        ),
      ),
    )
  );
}