import 'package:flutter/material.dart';
import 'package:tutomap/model/map_type_handler.dart';

import '../model/map_type.dart';

class DrawerTile extends StatelessWidget{

  final MapType mapType;
  final Function(MapType) onTap;

  const DrawerTile({
    required this.mapType,
    required this.onTap,
});

  @override
  Widget build(BuildContext context) {


    return ListTile(
      title: Text(MapTypeHandler().titleForType(mapType)),
      leading: Icon(MapTypeHandler().iconForType(mapType)),
      onTap: (() => onTap(mapType)),
    );
  }
}