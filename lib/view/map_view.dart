import 'package:flutter/cupertino.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:flutter_map_marker_popup/extension_api.dart' as pop;
import 'package:latlong2/latlong.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:tutomap/map/options/map_o.dart';
import 'package:tutomap/map/widgets/tile_layer_w.dart';
import 'package:tutomap/model/map_type.dart';
import '../model/map_type_handler.dart';
import '../model/remarquable_place.dart';

class MapView extends StatelessWidget{

  final LatLng center;
  final double zoom;
  final MapController mapController;
  final MapType mapType;
  final List<Marker> markers;
  pop.PopupController popController = pop.PopupController();
  Function(String) removeMarker;
  Function(RemarquablePlace) moveToDetail;

  MapView({
    Key? key,
    required this.mapController,
    required this.center,
    required this.zoom,
    required this.mapType,
    required this.markers,
    required this.moveToDetail,
    required this.removeMarker
  }): super(key: key);


  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      mapController: mapController,
        options: MapO(center: center, zoom: zoom),
      children: [
        TileLayerW(),
        CurrentLocationLayer(),
        MapTypeHandler().layerForType(mapType, markers, popController, moveToDetail, removeMarker)
      ],
    );
  }
}