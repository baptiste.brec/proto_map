import 'package:flutter_map/plugin_api.dart';
import 'package:latlong2/latlong.dart';

class MapO extends MapOptions{
  final LatLng center;
  final double zoom;

  MapO({
    required this.center,
    required this.zoom
}): super(
    center: center,
    zoom: zoom,
  );

}