import 'package:flutter_map/flutter_map.dart';

class TileLayerW extends TileLayer{
  TileLayerW(): super(
    urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
    subdomains: ['a', 'b', 'c'],
    userAgentPackageName: 'com.example.app',
  );
}