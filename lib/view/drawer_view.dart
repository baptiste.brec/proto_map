import 'package:flutter/material.dart';
import 'package:tutomap/model/map_type.dart';
import 'package:tutomap/view/drawer_header.dart';
import 'package:tutomap/view/drawer_tile.dart';

class DrawerView extends StatelessWidget{

  final Function(MapType) onDrawerTap;

  DrawerView({
    required this.onDrawerTap
});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
      child: Column(
        children: drawerList(),
      ),
    );
  }


  List<Widget> drawerList(){
    List<Widget> list= [CustomDrawerHeader()];
    list.addAll(MapType.values.map((mapType) => DrawerTile(mapType: mapType, onTap: onDrawerTap)).toList());
    return list;
  }
}