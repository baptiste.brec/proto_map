import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:flutter_map_marker_popup/extension_api.dart' as pop;
import 'package:tutomap/map/widgets/marker_layer_w.dart';
import 'package:tutomap/map/widgets/pop_w.dart';
import 'package:tutomap/model/map_type.dart';
import 'package:tutomap/model/remarquable_place.dart';

class MapTypeHandler {

  String titleForType(MapType mapType){
    switch (mapType){
      case MapType.simple: return "Simple";
      case MapType.annotations: return "Markers";
      case MapType.popup: return "Pop up";
      case MapType.cluster: return "Cluster";
      case MapType.clusterAndPop: return "Cluster et Pop-up";
    }
  }

  IconData iconForType(MapType mapType){
    switch (mapType){
      case MapType.simple: return Icons.map;
      case MapType.annotations: return Icons.location_on;
      case MapType.popup: return Icons.message;
      case MapType.cluster: return Icons.circle_sharp;
      case MapType.clusterAndPop:return Icons.supervised_user_circle_sharp;
    }
  }


  Widget layerForType(
      MapType mapType,
      List<Marker> markers,
      pop.PopupController popController,
      Function(RemarquablePlace) moveToDetail,
      Function(String) removeMarker,
      ) {
    switch (mapType){
      case MapType.simple: return Container();
      case MapType.annotations: return MarkerLayerW(markers: markers);
      case MapType.popup: return PopW(markers: markers, controller: popController, removeMarker: removeMarker, moveToDetail: moveToDetail,);
      case MapType.cluster: return Container();
      case MapType.clusterAndPop: return Container();
    }
  }

}