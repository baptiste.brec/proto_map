import 'package:latlong2/latlong.dart';

class RemarquablePlace{
  double lat;
  double lon;
  String city;
  String adress;




  LatLng get center => LatLng(lat, lon);
  String get toBeSavedString => "$lat&$lon";

  //setCity(String c) => _city=c;
  //setAdress(String a) => _adress=a;

  RemarquablePlace({
    required this.lat,
    required this.lon,
    required this.city,
    required this.adress
});


}