import 'package:flutter_map/flutter_map.dart';

class MarkerLayerW extends MarkerLayer{
  MarkerLayerW({required List<Marker> markers})
      : super(markers: markers);
}